import localizeFilter from "@/filters/localize.filter";

export default {
    install(Vue) {
        Vue.prototype.$title = function (titleKey) {
            const appName = process.env.VUE_APP_TITLE  //получаем название приложения из .env
            return `${localizeFilter(titleKey)} | ${appName}` // добавляем в тайтлах название страницы + название приложения через черту
        }
    }
}