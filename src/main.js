import Vue from 'vue'
import Vuelidate from "vuelidate/src";
import Paginate from 'vuejs-paginate'
import VueMeta from 'vue-meta'
import App from './App.vue'
import router from './router'
import store from './store'
import dateFilter from '@/filters/date.filter'
import currencyFilter from "@/filters/currency.filter";
import localizeFilter from "@/filters/localize.filter";
import tooltipDirective from "@/directives/tooltip.directives"
import messagePlugin from '@/utils/message.plugin'
import titlePlugin from '@/utils/title.plugin'
import Loader from "@/components/app/Loader";
import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'

import firebase from "firebase/app";
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false

Vue.use(messagePlugin)
Vue.use(titlePlugin)
Vue.use(Vuelidate)
Vue.use(VueMeta)
Vue.filter('date', dateFilter)
Vue.filter('currency', currencyFilter)
Vue.filter('localize', localizeFilter)
Vue.directive('tooltip', tooltipDirective)
Vue.component('Loader', Loader)
Vue.component('paginate', Paginate)

// Initialize Firebase
firebase.initializeApp(
    {
        apiKey: "AIzaSyBmhSvD9otDShcO-Ey67TZBkibNfadk6Kw",
        authDomain: "crm-system-first.firebaseapp.com",
        databaseURL: "https://crm-system-first.firebaseio.com",
        projectId: "crm-system-first",
        storageBucket: "crm-system-first.appspot.com",
        messagingSenderId: "533886899266",
        appId: "1:533886899266:web:feedd537499b791bd309db",
        measurementId: "G-GHDWH8FJZN"
    }
)

let app


firebase.auth().onAuthStateChanged(() => {
    if (!app) {
        app = new Vue({
            router,
            store,
            render: h => h(App)
        }).$mount('#app')
    }
})



