import _ from 'lodash'

export default {
    data() {
        return {
            page: +this.$route.query.page || 1, //  показываем ту страницу  на которой находились при перезагрузке или показываем первую страницу по дефолту
            pageSize: 5,  // размер страницы
            pageCount: 0, // по умолчанию нет никаких страниц
            allItems: [], // будем хранить все данные
            items: [] // будм хранить те данные, которые нам необходимо показівать
        }
    },
    methods: {
        pageChangeHandler(page) {
            this.$router.push(`${this.$route.path}?page=${page}`)  //добавляем параметр страницы в url
            this.items = this.allItems[page - 1] || this.allItems[0]
        },
        setupPagination(allItems) {
            this.allItems = _.chunk(allItems, this.pageSize)
            this.pageCount = _.size(this.allItems)
            this.items = this.allItems[this.page - 1] || this.allItems[0]
        }
    }

}